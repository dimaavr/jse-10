package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.ICommandRepository;
import ru.tsc.avramenko.tm.constant.ArgumentConst;
import ru.tsc.avramenko.tm.constant.TerminalConst;
import ru.tsc.avramenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer information."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display list arguments."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display list commands."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show a list of projects."
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Removing all projects."
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show a list of tasks."
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Removing all tasks."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, VERSION, ARGUMENTS, COMMANDS, HELP, EXIT,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR, TASK_LIST,
            TASK_CREATE, TASK_CLEAR
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
